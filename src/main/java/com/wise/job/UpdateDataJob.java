package com.wise.job;

import java.util.Date;

import com.wise.Service.CompanyUpdateService;
import com.wise.util.DateUtil;
import com.wise.util.SpringContext;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by JEAN on 2018/4/16.
 */
@Slf4j
@DisallowConcurrentExecution
public class UpdateDataJob implements BaseJob {

    private CompanyUpdateService companyUpdateService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        if(companyUpdateService == null){
            companyUpdateService = (CompanyUpdateService) SpringContext.getBeanByClass(CompanyUpdateService.class);
        }
        log.info("SFTP Update the latest Companies start,current time: "+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        companyUpdateService.doUpdate();
        log.info("SFTP Update the latest Companies start,current time: "+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
    }
}
