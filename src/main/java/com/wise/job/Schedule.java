package com.wise.job;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.alibaba.fastjson.JSON;
import com.wise.Service.DataSyncService;
import com.wise.config.CustomConfig;
import com.wise.entry.PutObject;
import com.wise.util.DateUtil;

import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import au.com.bytecode.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by JEAN on 2018/4/10.
 */
@Slf4j
@Component
public class Schedule {

    @Autowired
    OkHttpClient okHttpClient;

    @Autowired
    DataSyncService dataSyncService;


   // @Scheduled(fixedRate = 600*1000)
    public void doGetDate(){
        log.info("SFTP Download the latest data files start,current time: "+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        dataSyncService.downLoadDateFile();
        log.info("SFTP Download the latest data files end,current time: "+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
    }

//    @Scheduled(fixedRate = 60*1000)

}
