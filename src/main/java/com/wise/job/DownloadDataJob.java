package com.wise.job;

import java.util.Date;

import com.wise.Service.DataSyncService;
import com.wise.util.DateUtil;
import com.wise.util.SpringContext;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by JEAN on 2018/4/16.
 */
@Slf4j
@DisallowConcurrentExecution
public class DownloadDataJob implements BaseJob {

    private DataSyncService dataSyncService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        if(dataSyncService == null){
            dataSyncService = (DataSyncService) SpringContext.getBeanByClass(DataSyncService.class);
        }
        log.info("SFTP Download the latest data files start,current time: "+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        dataSyncService.downLoadDateFile();
        log.info("SFTP Download the latest data files end,current time: "+ DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));

    }
}
