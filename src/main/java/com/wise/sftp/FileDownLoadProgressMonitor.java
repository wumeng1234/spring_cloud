package com.wise.sftp;

import com.jcraft.jsch.SftpProgressMonitor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileDownLoadProgressMonitor implements SftpProgressMonitor {

    private long transfered;

    @Override
    public boolean count(long count) {
        transfered = transfered + count;
        if(transfered<1024){
            log.info("SFTP Currently transferred total size: " + transfered + " bytes");
        }
        if ((transfered> 1024) && (transfered<1048576)){
            log.info("SFTP Currently transferred total size: " + (transfered/1024) + "K bytes");
        }else{
            log.info("SFTP Currently transferred total size: " +( transfered/1024/1024) + "M bytes");
        }
        return true;
    }
    @Override
    public void end() {
        log.info("SFTP Transferring done.");
    }
    @Override
    public void init(int op, String src, String dest, long max) {
        log.info("SFTP Transferring begin.");
    }

}