package com.wise.sftp;

import lombok.Data;

/**
 * Created by JEAN on 2018/4/12.
 */
@Data
public abstract class FileFilter {
    private String prefix;
    private String suffix;
    private String extend;
    public abstract boolean customFilter(String fileName,int modifyTime);

}
