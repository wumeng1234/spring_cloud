package com.wise.sftp;

import lombok.Data;

/**
 * Created by JEAN on 2018/4/13.
 */

@Data
public class FileInfo {
    private String name;
    private long size;
    private int mtime;
}
