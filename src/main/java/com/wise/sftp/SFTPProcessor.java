package com.wise.sftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Proxy;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.wise.config.CustomConfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by JEAN on 2018/4/12.
 */
@Slf4j
public class SFTPProcessor {

    private  Session session = null;

    private  ChannelSftp channel = null;

    public  ChannelSftp getChannel(){

        final String host = CustomConfig.getStringProp("data.sftp.host");
        final Integer port = CustomConfig.getIntegerProp("data.sftp.port");
        final String name = CustomConfig.getStringProp("data.sftp.name");
        final String password = CustomConfig.getStringProp("data.sftp.password");
        final Boolean havProxy = CustomConfig.getBooleanProp("data.sftp.proxy");
        final String proxyHost = CustomConfig.getStringProp("data.sftp.proxy.host");
        final Integer proxyPort = CustomConfig.getIntegerProp("data.sftp.proxy.port");
        final Integer timeOut = CustomConfig.getIntegerProp("data.sftp.timeout",600000);

        JSch jsch = new JSch();

        try {
            log.info("SFTP [ Host: "+host+" Port: "+port+" User: "+name+" ]");
            session = jsch.getSession(name,host,port);
            if(havProxy){
                Proxy proxy = new ProxyHTTP(proxyHost,proxyPort);
                session.setProxy(proxy);
                log.info("SFTP Proxy [ ProxyHost: "+proxyHost+" ProxyPort: "+proxyPort+" ]");
            }
            session.setPassword(password);
            session.setTimeout(timeOut);
            session.setConfig("StrictHostKeyChecking","no");
            session.setConfig("UseDNS","no");
            session.connect();
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();

            return  channel;


        } catch (JSchException e) {
            log.error("SFTP Get channel exception", e);
            e.printStackTrace();
        }

        return null;

    }

    public  void closeChannel() throws Exception {
        try {
            if (channel != null) {
                channel.disconnect();
            }
            if (session != null) {
                session.disconnect();
            }
        } catch (Exception e) {
            log.error("SFTP Close channel exception", e);
            throw e;
        }
    }


    public  void uploadFile(String localFile, String newName, String remoteFoldPath) throws Exception{
        InputStream input = null;
        try {
            input = new FileInputStream(new File(localFile));
            channel.cd(remoteFoldPath);
            channel.put(input, newName);
        } catch (Exception e) {
            log.error("SFTP UpLoad file error: ", e);
            throw e;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error("SFTP Close stream error.");
                    throw new Exception("SFTP stream error.");
                }
            }
        }
    }

    public  void downloadFile(String remoteFile, String remotePath,long remoteFileSize, String localFile,Object repeatDownLoad) throws Exception {
        log.info("SFTP Download file remotePath : "+remotePath+File.separator+remoteFile+" to localPath : "+localFile+" !");
        OutputStream output = null;
        File file = null;
        try {
            file = new File(localFile);
            if (!file.exists()) {
                file.createNewFile();
            }else{
                if(repeatDownLoad instanceof Boolean){
                    if(!((Boolean) repeatDownLoad).booleanValue()){
                        log.info("SFTP Files already exist and are not repeated Downloads");
                        return;
                    }
                }
                if(repeatDownLoad instanceof RepeatDownLoadCondition){
                    if(!((RepeatDownLoadCondition)repeatDownLoad).repeatDownLoad()){
                        log.info("SFTP Files already exist and are not repeated Downloads");
                        return;
                    }
                }

                log.info("SFTP The file has already existed and repeated Downloads");
            }
            output = new FileOutputStream(file);
            channel.cd(remotePath);
            channel.get(remoteFile,output,new FileProgressMonitor(remoteFileSize));
        } catch (Exception e) {
            log.error("SFTP Download file error: ", e);
            throw e;
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    log.error("SFTP Close stream error.");
                    throw e;
                }
            }

        }
    }

    public  List listFiles(String remotePath) throws Exception {
       return listFiles(remotePath,null);
    }

    public Vector listFilesEx(String remotePath) throws Exception {
        Vector vector = null;
        try {
            vector = channel.ls(remotePath);
        } catch (SftpException e) {
            log.error("SFTP List file error", e);
            throw new Exception("SFTP List file error.");
        }
        return vector;
    }


    public  List<FileInfo> listFiles(String remotePath, FileFilter filter) throws Exception {
        final List<FileInfo> vector = new ArrayList<>();
        try {
            ChannelSftp.LsEntrySelector selector = new ChannelSftp.LsEntrySelector() {
                @Override
                public int select(ChannelSftp.LsEntry lsEntry) {
                    String fileName = lsEntry.getFilename();
                    FileInfo fileInfo = new FileInfo();
                    fileInfo.setName(fileName);
                    fileInfo.setMtime(lsEntry.getAttrs().getMTime());
                    fileInfo.setSize(lsEntry.getAttrs().getSize());
                    if(fileName.endsWith(".")||fileName.endsWith("..")){
                        return 0;
                    }
                    if(filter == null){
                        vector.add(fileInfo);
                    }else{
                        if(!StringUtils.isEmpty(filter.getPrefix())){
                            if(!fileName.startsWith(filter.getPrefix())){
                               return 0;
                            }
                        }
                        if(!StringUtils.isEmpty(filter.getSuffix())){
                            if(!fileName.substring(0,fileName.lastIndexOf(".")).endsWith(filter.getSuffix())){
                                return 0;
                            }
                        }
                        if(!StringUtils.isEmpty(filter.getExtend())){
                            if(!fileName.endsWith(filter.getExtend())){
                                return 0;
                            }
                        }
                        if(!filter.customFilter(fileName,lsEntry.getAttrs().getMTime())){
                           return 0;
                        }
                        vector.add(fileInfo);
                    }
                    return 0;
                }
            };
            channel.ls(remotePath,selector);
        } catch (SftpException e) {
            log.error("SFTP List file error", e);
            e.printStackTrace();
            throw new Exception("SFTP List file error.");
        }
        return vector;
    }



}
