package com.wise.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.wise.config.CustomConfig;
import com.wise.entry.PutObject;

import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVReader;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by JEAN on 2018/4/16.
 */
@Slf4j
@Service
public class CompanyUpdateService {
    @Autowired
    OkHttpClient okHttpClient;

    @Autowired
    DataSyncService dataSyncService;

    public void doUpdate(){

        String pushFilePath = String.valueOf(CustomConfig.getProp("data.push.dir"));

        log.info("Push file path："+pushFilePath);

        File[]  pushFiles = new File(pushFilePath).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".csv");
            }
        });



        if(pushFiles == null && pushFiles.length <=0){
            log.info("Push file is not exist");
            return;
        }

        for (File pushFile : pushFiles) {
            if(!pushFile.getName().endsWith(".csv")){
                continue;
            }
            updateHandler(pushFile);
        }
    }

    public void updateHandler(File pushFile){
        try {
            final String fileName= pushFile.getName();
            log.info("Push file company name "+fileName);
            List<PutObject> putObjects = new ArrayList<>();

            /**
             * 读取文件
             */
            InputStreamReader fReader = new InputStreamReader(new FileInputStream(pushFile),"UTF-8");
            CSVReader csvReader  = new CSVReader(fReader,',');
            String[] tmp ;
            while ((tmp = csvReader.readNext()) != null){
                log.info("update company name:"+tmp[0]);
                PutObject putObject = new PutObject();
                putObject.setCompanyName(tmp[0]);
                putObject.setChanleId(tmp[1]);
                putObjects.add(putObject);
            }


            csvReader.close();
            fReader.close();

            renameFile(pushFile.getPath(),pushFile.getPath().replace(".csv",".complete"));

            FormBody.Builder params = new FormBody.Builder();
            params.add("token", String.valueOf(CustomConfig.getProp("data.update.token")));

            params.add("companys", Base64.encodeBase64String((JSON.toJSONString(putObjects)).getBytes("UTF-8")));
            Request request=new Request.Builder()
                    .url(String.valueOf(CustomConfig.getProp("data.update.url")))
                    .post(params.build())
                    .build();
            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    log.info("update company fail!");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    InputStream is = null;
                    byte[] buf = new byte[2048];
                    int len = 0;
                    FileOutputStream fos = null;
                    // 储存下载文件的目录
                    String savePath = String.valueOf(CustomConfig.getProp("data.zip.dir"));
                    try {
                        log.info("update company request code:"+response.code());
                        is = response.body().byteStream();
                        File file = new File(savePath, fileName.replace(".csv", ".zip"));
                        fos = new FileOutputStream(file);
                        while ((len = is.read(buf)) != -1) {
                            fos.write(buf, 0, len);
                        }
                        fos.flush();
                        log.info("update company success!");
                    } catch (Exception e) {
                        log.info("update company exception!",e);
                        throw e;
                    } finally {
                        try {
                            if (is != null)
                                is.close();
                        } catch (IOException e) {
                            log.info("update company exception!",e);
                        }
                        try {
                            if (fos != null)
                                fos.close();
                        } catch (IOException e) {
                            log.info("update company exception!",e);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            log.info("更新下载企业信息异常",e);
        }

    }

    public static File renameFile(String filePath,String newFilePath){
        File oldfile=new File(filePath);
        File newfile=new File(newFilePath);
        if(!oldfile.exists()){
            return null;
        }
        if(newfile.exists())
            return null;
        else{
            if(!oldfile.renameTo(newfile)){
                return null;
            };
        }
        return newfile;
    }
}
