package com.wise.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wise.config.CustomConfig;
import com.wise.sftp.FileFilter;
import com.wise.sftp.FileInfo;
import com.wise.sftp.RepeatDownLoadCondition;
import com.wise.sftp.SFTPProcessor;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by JEAN on 2018/4/12.
 */

@Slf4j
@Service
public class DataSyncService {

    public boolean downLoadDateFile(){
        final String zipFileDir = CustomConfig.getStringProp("data.zip.dir");
        final String[] fileDirs = CustomConfig.getStringProp("data.downLoad.dir").split(",");
        final int bt = CustomConfig.getIntegerProp("data.downLoad.boundaryTime",15);
        final Map<String,List<FileInfo>> validFiles = new HashMap<>();
        try {

            SFTPProcessor sftp = new SFTPProcessor();
            sftp.getChannel();

            log.info("SFTP Check the data files for the last "+bt+" days");
            int boundaryTime = getBoundaryTime(-1*bt);

            FileFilter filter = new FileFilter() {
                @Override
                public boolean customFilter(String fileName, int modifyTime) {
                    if(modifyTime < boundaryTime){
                        return false;
                    }
                    return true;
                }
            };
            filter.setExtend(".zip");
            for (String fileDir : fileDirs) {
                List<FileInfo> files = sftp.listFiles(fileDir,filter);
                for (FileInfo file : files) {
                    log.info("FileDir: "+fileDir+file.getName());
                }
                validFiles.put(fileDir,files);
            }

            for (String dir : validFiles.keySet()) {
                File tmpDir = new File(zipFileDir+dir);
                if(!tmpDir.exists()){
                    tmpDir.mkdirs();
                }
                for (FileInfo vfile : validFiles.get(dir)) {
                    String localFile =  zipFileDir+dir+vfile.getName();
                    sftp.downloadFile(vfile.getName(),dir,vfile.getSize(),localFile, new RepeatDownLoadCondition() {
                        @Override
                        public boolean repeatDownLoad() {
                            File file = new File(localFile);
                            /*File process = new File(localFile.replace(".zip",".process"));
                            if(process.exists()){
                                log.info("SFTP The file: ["+vfile.getName()+"] is being downloaded and does not need to be downloaded again");
                                return false;
                            }*/
                            if(file.exists()){
                              return (file.length() < vfile.getSize());
                            }
                            return false;
                        }
                    });
                }
            }
            sftp.closeChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

   /* //判断文件是否在处理中
    public boolean check(String filePath,String name){
        synchronized (this) {
           File tmp = new File(filePath+name.replace(".zip",".process"));
           if(tmp.exists()){
               return false;
           }
            try {
                tmp.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }*/

    public int getBoundaryTime(int bt){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE,bt);
       return Integer.parseInt(String.valueOf(calendar.getTime().getTime()/1000));
    }

}
