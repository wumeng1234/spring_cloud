package com.wise.entry;

import java.io.Serializable;

/**
 * Created by JEAN on 2018/4/9.
 */
public class PutObject implements Serializable {
    private static final long serialVersionUID = 3004948814960810583L;
    private String companyName;

    private String chanleId;

    public String getChanleId() {
        return chanleId;
    }

    public void setChanleId(String chanleId) {
        this.chanleId = chanleId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}
