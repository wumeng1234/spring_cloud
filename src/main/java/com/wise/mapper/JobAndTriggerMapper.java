package com.wise.mapper;

import java.util.List;

import com.wise.entry.JobAndTrigger;

public interface JobAndTriggerMapper {
	public List<JobAndTrigger> getJobAndTriggerDetails();
}
