package com.wise;

import com.wise.config.CustomConfig;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;


@ComponentScan
@SpringBootApplication
@MapperScan("com.wise.mapper")
/*@EnableScheduling*/
public class HttpExtDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpExtDataApplication.class, args);
	}
}
