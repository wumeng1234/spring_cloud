package com.wise.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by JEAN on 2017/10/9.
 */
@Slf4j
public class CustomConfig {

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomConfig.class);

    private static Properties properties;
    private final static Map<String,Object> mapProps = new HashMap<>();
    public static  void setProp(String key,Object value){
        mapProps.put(key,value);
    }
    public static Object getProp(String key){
        return mapProps.get(key);
    }

    static {
        init();
    }

    public static Object getProp(String key,Object defaultValue){
        Object object = mapProps.get(key);
        if(object == null){
            object = defaultValue;
        }
        return object;
    }

    public static String getStringProp(String key){
        return  String.valueOf(getProp(key,""));
    };
    public static String getStringProp(String key,Object defaultValue){
        return  String.valueOf(getProp(key,defaultValue));
    };

    public static Integer getIntegerProp(String key){
        return getIntegerProp(key,0);
    };
    public static Integer getIntegerProp(String key,Object defaultValue){
        return  Integer.parseInt(getStringProp(key,defaultValue));
    };

    public static Boolean getBooleanProp(String key){
        return getBooleanProp(key,false);
    };
    public static Boolean getBooleanProp(String key,Object defaultValue){
        return  Boolean.parseBoolean(getStringProp(key,defaultValue));
    };

    public static Double getDoubleProp(String key){
        return getDoubleProp(key,0.0);
    };
    public static Double getDoubleProp(String key,Object defaultValue){
        return  Double.parseDouble(getStringProp(key,defaultValue));
    };

    public static Float getFloatProp(String key){
        return getFloatProp(key,0.0);
    };
    public static Float getFloatProp(String key,Object defaultValue){
        return  Float.parseFloat(getStringProp(key,defaultValue));
    };

    public static void init(){
        LOGGER.info("load data dictionary");
        if (properties == null){
            properties = new Properties();
        }

        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        if(rootPath.indexOf(".jar") > 0){
            rootPath = rootPath.substring(0,rootPath.indexOf(".jar"));
            rootPath = rootPath.substring(0,rootPath.lastIndexOf("/")+1).replace("file:/","");
        }
        log.info("load data dictionary："+rootPath+"config/custom-config.properties");
        File dataFile = new File(rootPath+"config/custom-config.properties");
        if (dataFile.exists()){
            try {
                FileInputStream in = new FileInputStream(dataFile);
                properties.load(new InputStreamReader(in,"UTF-8"));
                for (Object o : properties.keySet()) {
                    LOGGER.info("KEY:"+String.valueOf(o)+" VALUE:"+properties.get(o));
                    setProp(String.valueOf(o),properties.get(o));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
