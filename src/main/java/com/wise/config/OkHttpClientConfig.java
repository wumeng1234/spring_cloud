package com.wise.config;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import okhttp3.OkHttpClient;


@Configuration
public class OkHttpClientConfig {
    @Bean
    public OkHttpClient okHttpClient(){
        final String ip = String.valueOf(CustomConfig.getProp("data.update.proxy.ip"));
        final int port = Integer.parseInt(String.valueOf(CustomConfig.getProp("data.update.proxy.port")));
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
                if(Boolean.parseBoolean(String.valueOf(CustomConfig.getProp("data.update.proxy")))){
                    builder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip,port)));
                }
        OkHttpClient client = builder.connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.MINUTES)
                .writeTimeout(30,TimeUnit.MINUTES)
                .build();
        return client;
    }
}